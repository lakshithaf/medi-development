<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SchoolUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'School Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>







   <!-- <?php /*$form = ActiveForm::begin(['id' => 'activations-form']); */?>
    <div class="col-xs-12">
        <div class="col-sm-12 col-lg-12" style="margin-left: 116px;" >

            <div class="col-sm-3 col-lg-3">
                <?/*= $form->field($model, 'activated_from')->input('date'); */?>
            </div>
            <div class="col-sm-3 col-lg-3">
                <?/*= $form->field($model, 'activated_to')->input('date'); */?>
            </div>
            <div class="col-sm-3 col-lg-3">
                <div class="btn_submit_report" style="margin-top: 25px;">
                    <?/*= Html::submitButton('<span class="glyphicon glyphicon-eye-open" >&nbsp;</span>View', ['class' => 'btn btn-primary ']); */?>
                    <button class="btn btn-primary"><span class="glyphicon  glyphicon-fullscreen"></span> Reset</button>
                </div>
            </div>

        </div>
    </div>
    <?php /* $form = ActiveForm::end(['id' => 'offline-month-form'])*/?>
    <br><br><br><br>-->



    <?php $form = ActiveForm::begin(['id' => 'activations-form']); ?>

    <?php  $form = ActiveForm::end(['id' => 'offline-month-form'])?>







    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'auth',
            'confirmed',
            //'policyagreed',
            //'deleted',
            // 'suspended',
            // 'mnethostid',
             'username',
            // 'password',
            // 'idnumber',
             'firstname',
             'lastname',
             'email:email',
            // 'emailstop:email',
            // 'icq',
            // 'skype',
            // 'yahoo',
            // 'aim',
            // 'msn',
             'phone1',
            // 'phone2',
            // 'institution',
            // 'department',
            // 'address',
            // 'city',
            // 'country',
            // 'lang',
            // 'calendartype',
            // 'theme',
            // 'timezone',
            // 'firstaccess',
             'lastaccess:datetime',
             'lastlogin:datetime',
            // 'currentlogin',
            // 'lastip',
            // 'secret',
            // 'picture',
            // 'url:url',
            // 'description:ntext',
            // 'descriptionformat',
            // 'mailformat',
            // 'maildigest',
            // 'maildisplay',
            // 'autosubscribe',
            // 'trackforums',
             'timecreated'=>[
                'attribute' => 'timecreated',
                 'value' => 'timecreated',
                 'format' => 'datetime',
                'filter' => DateRangePicker::widget([
                 'model' => $searchModel,
                 'attribute' => 'timecreated',
                 'convertFormat' => true,
                 'pluginOptions' => [
                     'locale' => [
                         'format' => 'Y-m-d'
                     ],
                 ],
             ]),]

            // 'timemodified:datetime',
            // 'trustbitmask',
            // 'imagealt',
            // 'lastnamephonetic',
            // 'firstnamephonetic',
            // 'middlename',
            // 'alternatename',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
