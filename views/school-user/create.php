<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SchoolUser */

$this->title = 'Create School User';
$this->params['breadcrumbs'][] = ['label' => 'School Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
