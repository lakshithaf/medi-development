<?php
/**
 * Created by PhpStorm.
 * User: Dell-PC
 * Date: 8/12/2016
 * Time: 2:43 PM
 */

use yii\widgets\ActiveForm;
use yii\grid\GridView;
use \yii\data\ArrayDataProvider;
use yii\helpers\Html;

$this->title = 'User Registration Flow';

?>
<h1><?= Html::encode($this->title) ?></h1>
<?php $form = ActiveForm::begin(['id' => 'user-flow-form']); ?>
<div class="col-xs-12">
    <div class="col-sm-12 col-lg-12">

        </br></br>

        <div class="col-sm-3 col-lg-3">
        </div>
        <div class="col-sm-3 col-lg-3">
            <?= $form->field($model, 'from')->input('date'); ?>
        </div>
        <div class="col-sm-3 col-lg-3">
            <?= $form->field($model, 'to')->input('date'); ?>
        </div>
        <div class="col-sm-3 col-lg-3">
            <div class="btn_submit_report" style="margin-top: 25px;">
                <?= Html::submitButton('<span class="glyphicon glyphicon-eye-open" ></span> View', ['class' => 'btn btn-primary ']); ?>
            </div>
        </div>
    </div>
</div>
<?php $form = ActiveForm::end(['id' => 'user-flow-form']); ?>

<br><br><br><br><br><br>

<div class="col-sm-12 col-lg-12">
    <h4>Mobile Registration</h4>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => ['date', 'mobitel_stage_1', 'dialog_stage_1', 'mobitel_stage_2', 'dialog_stage_2'],
    ]); ?>
</div>
<br><br><br><br><br><br><br><br>


<div class="col-sm-12 col-lg-12">
    <h4>Email Registration</h4>
    <?= GridView::widget([
        'dataProvider' => $dataProvider1,
        'columns' => ['date', 'mobitel_stage_1', 'dialog_stage_1', 'mobitel_stage_2', 'dialog_stage_2'],
    ]); ?>
</div>


<br><br><br><br><br><br><br><br>


<div class="col-sm-12 col-lg-12">
    <h4>Social Registration</h4>
    <?= GridView::widget([
        'dataProvider' => $dataProvider2,
        'columns' => ['date', 'mobitel_stage_1', 'dialog_stage_1', 'mobitel_stage_2', 'dialog_stage_2'],
    ]); ?>

</div>