<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Student */
/* @var $form yii\widgets\ActiveForm */

$district=['Colombo' => 'Colombo', 'Gamapaha' => 'Gamapaha','Kurunegala' => 'Kurunegala', 'Kandy' => 'Kandy','Matale' => 'Matale',
    'Nuwara Eliya' =>'Nuwara Eliya', 'Ampara' => 'Ampara','Batticaloa' => 'Batticaloa', 'Trincomalee' => 'Trincomalee','Anuradhapura' => 'Anuradhapura',
    'Polonnaruwa' =>'Polonnaruwa', 'Jaffna' => 'Jaffna','Kilinochchi' => 'Kilinochchi', 'Mannar' => 'Mannar','Mullaitivu' => 'Mullaitivu',
    'Vavuniya' => 'Vavuniya', 'Puttalam' => 'Puttalam','Kegalle' => 'Kegalle', 'Ratnapura' => 'Ratnapura','Galle' => 'Galle',
    'Hambantota' => 'Hambantota','Matara' => 'Matara', 'kaluthara' => 'kaluthara', 'Badulla' => 'Badulla','Monaragala' => 'Monaragala'];

?>

<div class="student-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'stu_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_line1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_line2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_line3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'student_tp')->textInput() ?>

    <?= $form->field($model, 'district')->dropDownList($district,['prompt'=>'Select Option']);  ?>

    <?= $form->field($model, 'al_year')->textInput() ?>

    <?= $form->field($model, 'faculty_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gurdian_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'occupation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gurdian_tp')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
