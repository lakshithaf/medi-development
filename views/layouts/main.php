<?php
/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use webvimark\modules\UserManagement\components\GhostMenu;
use webvimark\modules\UserManagement\UserManagementModule;
use yii\filters\AccessControl;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <?php


    NavBar::begin([
        'brandLabel' => "Medical Faculty Students' Action Committee",
        'brandUrl' => Yii::$app->homeUrl,
    ]);


    echo GhostMenu::widget([
        'encodeLabels'=>false,
        'activateParents'=>true,
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            [
                'label' => 'Authentication',
                'items'=>UserManagementModule::menuItems()
            ],

            [
                'label' => 'Student Management',
                'items'=>[
                      ['label'=>'Student Details', 'url' => ['/student-search']],
                      ['label'=>'CSV Data Upload', 'url' => ['/excel-upload']],
                      ['label'=>'Change password', 'url'=>['/user-management/auth/change-own-password']],
                      ['label'=>'Logout', 'url'=>['/user-management/auth/logout']],

                ],



            ],

        ],
    ]);
   NavBar::end();
    ?>
    <div class="container">
        <?php
        $r = Yii::$app->session->getAllFlashes();
        $messages = Yii::$app->session->getAllFlashes();
        foreach ($messages as $key => $message) {
            if (is_array($message)) {
                foreach ($message as $m) {
                    echo '<div   class="alert alert-' . $key . '">' . $m . '</div>';
                }
            }
        }
        ?>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])
        ?>
        <?= $content ?>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= date('Y') ?> Medical Faculty Students' Action Committee.</p>
    </div>
</footer>
<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>


