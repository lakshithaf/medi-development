<?php
\bedezign\yii2\audit\web\JSLoggingAsset::register($this);
/* @var $this yii\web\View */

use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;

$this->title = 'Professional Report';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php

    echo Highcharts::widget([
        'options' => [
            'chart' => [
                'zoomType' => 'x'
            ],
            'title' => [
                'text' => 'Activations'
            ],
            'xAxis' => [
                'type' => 'datetime'
            ],
            'yAxis' => [
                'title' => [
                    'text' => 'Activations'
                ]
            ],
            'series' => [[
                'type' => 'area',
                'name' => 'Activations',
                'data' => $activations
            ],[
                'type' => 'area',
                'name' => 'Deactivations',
                'data' => $deactivations
            ]]
        ]
    ]);

    echo '</ br>';
    echo Highcharts::widget([
        'options' => [
            'chart' => [
                'zoomType' => 'x'
            ],
            'title' => [
                'text' => 'Revenue'
            ],
            'xAxis' => [
                'type' => 'datetime'
            ],
            'yAxis' => [
                'title' => [
                    'text' => 'Amount (Rs)'
                ]
            ],
            'series' => [[
                'type' => 'area',
                'name' => 'Charging',
                'data' => $charging
            ]]
        ]
    ]);

    //echo json_encode($activations);
    ?>

    <code><?= __FILE__ ?></code>
</div>
