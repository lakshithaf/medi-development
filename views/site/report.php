<?php
\bedezign\yii2\audit\web\JSLoggingAsset::register($this);
/* @var $this yii\web\View */

use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;

$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h2>Daily Report</h2>

    <?php

    echo Highcharts::widget([
        'options' => [
            'chart' => [
                'zoomType' => 'x'
            ],
            'title' => [
                'text' => 'Activations Daily'
            ],
            'xAxis' => [
                'type' => 'datetime'
            ],
            'yAxis' => [
                'title' => [
                    'text' => 'Activations'
                ]
            ],
            'series' => [[
                'type' => 'area',
                'name' => 'Activations',
                'data' => $activations
            ],[
                'type' => 'area',
                'name' => 'Deactivations',
                'data' => $deactivations
            ]]
        ]
    ]);

    echo '</ br>';
    echo Highcharts::widget([
        'options' => [
            'chart' => [
                'zoomType' => 'x'
            ],
            'title' => [
                'text' => 'Revenue Daily'
            ],
            'xAxis' => [
                'type' => 'datetime'
            ],
            'yAxis' => [
                'title' => [
                    'text' => 'Amount (Rs)'
                ]
            ],
            'series' => [[
                'type' => 'area',
                'name' => 'Charging',
                'data' => $charging
            ]]
        ]
    ]);

    //echo json_encode($activations);
    ?>

    </br >
    <h2>Monthly Report</h2>


    <?php


    echo Highcharts::widget([
        'options' => [
            'chart' => [
                'zoomType' => 'x',
                'type' => 'line'
            ],
            'title' => [
                'text' => 'Activations Monthly'
            ],
            'xAxis' => [
                'categories' => ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            ],
            'yAxis' => [
                'title' => [
                    'text' => 'Activations'
                ]
            ],
            'series' => [[
                'type' => 'area',
                'name' => 'Activations',
                'data' => $activationsm
            ],[
                'type' => 'area',
                'name' => 'Deactivations',
                'data' => $deactivationsm
            ]]
        ]
    ]);



    echo '</ br>';
    echo Highcharts::widget([
        'options' => [
            'chart' => [
                'zoomType' => 'x',
                'type' => 'line'
            ],
            'title' => [
                'text' => 'Revenue Monthly'
            ],
            'xAxis' => [
                'categories' => ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            ],
            'yAxis' => [
                'title' => [
                    'text' => 'Amount (Rs)'
                ]
            ],
            'series' => [[
                'type' => 'area',
                'name' => 'Charging',
                'data' => $chargingm
            ]]
        ]
    ]);

    //echo json_encode($activations);
    ?>

</div>

