<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $confirmation_token
 * @property integer $status
 * @property integer $superadmin
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $registration_ip
 * @property string $bind_to_ip
 * @property string $email
 * @property integer $email_confirmed
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'created_at', 'updated_at'], 'required'],
            [['status', 'superadmin', 'created_at', 'updated_at', 'email_confirmed'], 'integer'],
            [['username', 'password_hash', 'confirmation_token', 'bind_to_ip'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['registration_ip'], 'string', 'max' => 15],
            [['email'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'confirmation_token' => 'Confirmation Token',
            'status' => 'Status',
            'superadmin' => 'Superadmin',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'registration_ip' => 'Registration Ip',
            'bind_to_ip' => 'Bind To Ip',
            'email' => 'Email',
            'email_confirmed' => 'Email Confirmed',
        ];
    }

    public function savedata()
    {

        $email_id= Yii::$app->db->createCommand()->insert('medi_act.user', [

            'id'=> $this->id,
            'username'=> $this->username,
            'auth_key' => $this->auth_key,
            'password_hash'=> $this->password_hash,
            'confirmation_token' => $this->confirmation_token,
            'status' => 1,
            'superadmin' => $this->superadmin,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'registration_ip' => $this->registration_ip,
            'bind_to_ip' => $this->bind_to_ip,
            'email' => $this->email,
            'email_confirmed' => 0,

        ])->execute();



        return $email_id;



    }

    public function saveAuth()
    {
        $useridarray=   Yii::$app->db->createCommand('SELECT  `id` ,`superadmin` FROM `user` ORDER BY `id` DESC LIMIT 1')->queryAll();
        $isAdmin=$useridarray[0]['superadmin'];
        $userid=$useridarray[0]['id'];


        $auth_type=($isAdmin)== 1 ?  'admin' : 'user' ;
        $auth_assign= Yii::$app->db->createCommand()->insert('medi_act.auth_assignment', [
            'item_name'=> $auth_type,
            'user_id'=> $userid,
            'created_at' => date("Y/m/d")
        ])->execute();



       return $auth_assign;


    }



}
