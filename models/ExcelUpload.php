<?php
/**
 * Created by PhpStorm.
 * User: Dell-PC
 * Date: 8/12/2016
 * Time: 2:43 PM
 */

namespace app\models;

use Yii;
use yii\base\Model;



class ExcelUpload extends Model
{
    public $file;

    public function rules(){
        return [
           
            [['file'],'file','extensions'=>'csv','maxSize'=>1024 * 1024 * 5],
        ];
    }

    public function attributeLabels(){
        return [
            'file'=>'Select File',
        ];
    }




}