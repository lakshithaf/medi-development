<?php
/*
 * Created by Hashan Alwis
 * Monday, ‎April ‎25, ‎2016
 * */
namespace app\models;

use Yii;

/**
 * This is the model class for table "activations".
 *
 * @property integer $id
 * @property string $lms
 * @property integer $enrolid
 * @property integer $courseid
 * @property integer $userid
 * @property string $method
 * @property string $type
 * @property double $price
 * @property string $status
 * @property integer $activated_on
 * @property string $agent
 * @property string $coupon
 * @property string $extra1
 * @property string $extra2
 * @property string $extra3
 */
class Dashboard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lms', 'enrolid', 'courseid', 'userid', 'method', 'type', 'price', 'status', 'activated_on', 'agent', 'coupon', 'extra1', 'extra2', 'extra3'], 'required'],
            [['enrolid', 'courseid', 'userid', 'activated_on'], 'integer'],
            [['price'], 'number'],
            [['extra1', 'extra2', 'extra3'], 'string'],
            [['lms', 'method', 'type', 'status', 'agent', 'coupon'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lms' => 'Lms',
            'enrolid' => 'Enrolid',
            'courseid' => 'Courseid',
            'userid' => 'Userid',
            'method' => 'Method',
            'type' => 'Type',
            'price' => 'Price',
            'status' => 'Status',
            'activated_on' => 'Activated On',
            'agent' => 'Agent',
            'coupon' => 'Coupon',
            'extra1' => 'Extra1',
            'extra2' => 'Extra2',
            'extra3' => 'Extra3',
        ];
    }


}
