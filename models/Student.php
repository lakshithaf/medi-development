<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student".
 *
 * @property integer $id
 * @property string $stu_name
 * @property string $address_line1
 * @property string $address_line2
 * @property string $address_line3
 * @property integer $student_tp
 * @property string $gurdian_name
 * @property string $district
 * @property string $al_year
 * @property string faculty_name
 * @property string $occupation
 * @property integer $gurdian_tp
 */
class Student extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */




    public static function tableName()
    {
        return 'student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stu_name', 'address_line1', 'address_line2','student_tp','district','faculty_name', 'gurdian_name','gurdian_tp'], 'required'],
            [['student_tp','al_year', 'gurdian_tp'], 'integer'],
            [['stu_name','faculty_name', 'gurdian_name'], 'string', 'max' => 70],
            [['address_line1', 'address_line2', 'address_line3','district','occupation'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'stu_name' => 'Student Name',
            'address_line1' => 'Address Line1',
            'address_line2' => 'Address Line2',
            'address_line3' => 'Address Line3',
            'student_tp' => 'Student Tel',
            'district' => 'District',
            'al_year' => 'A/L Year',
            'faculty_name' => 'Faculty Name',
            'gurdian_name' => 'Guardian Name',
            'occupation' => 'Occupation',
            'gurdian_tp' => 'Gurdian Tel',

        ];
    }

    public function csvupload()
    {

        $email_id= Yii::$app->db->createCommand()->insert('student', [
            'id' => 'ID',
            'stu_name' => $this->stu_name,
            'address_line1' => $this->address_line1,
            'address_line2' => $this->address_line2,
            'address_line3' => $this->address_line3,
            'student_tp' => $this->student_tp,
            'district' => $this->district,
            'al_year' => $this->al_year,
            'faculty_name' =>$this->faculty_name,
            'gurdian_name' => $this->gurdian_name,
            'occupation' => $this->occupation,
            'gurdian_tp' => $this->gurdian_tp
        ])->execute();

        return $email_id;

    }


}
