<?php
/**
 * Created by PhpStorm.
 * User: Dell-PC
 * Date: 8/12/2016
 * Time: 2:43 PM
 */

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\base\Model;

class UserFlow extends \yii\db\ActiveRecord
{

    public $from;
    public $to;

    public static function tableName()
    {
        return 'guru_lms.fop_activations';
    }

    public function rules()
    {
        return [
            [['from', 'to', 'type'], 'string']
        ];

    }


}