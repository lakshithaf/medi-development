<?php
/*
 * Updated by Hashan Alwis
 * Monday, ‎April ‎25, ‎2016
 * */
namespace app\controllers;


use Yii;
use app\models\ExcelUpload;
use app\models\Student;
use yii\web\Controller;
use yii\web\UploadedFile;




class ExcelUploadController extends Controller
{
    public function actionIndex()
    {

        $model = new ExcelUpload();

        if($model->load(Yii::$app->request->post())){
            $file = UploadedFile::getInstance($model,'file');
            $filename = 'Data.'.$file->extension;
             $path = Yii::getAlias('@root').'/var/www/html/medi_dev/web/upload/';
            $upload = $file->saveAs($path.$filename);
            if($upload){
                $csv_file = $path.$filename;
                $filecsv = file($csv_file);
                foreach($filecsv as $data){
                    $modelnew = new Student;
                    $dataarry= explode(",",$data);
                    $stu_name = $dataarry[0];
                    $address_line1 = $dataarry[1];
                    $address_line2 = $dataarry[2];
                    $address_line3 = $dataarry[3];
                    $student_tp = $dataarry[4];
                    $district = $dataarry[5];
                    $al_year = $dataarry[6];
                    $faculty_name = $dataarry[7];
                    $gurdian_name = $dataarry[8];
                    $occupation = $dataarry[9];
                    $gurdian_tp = $dataarry[10];
                    $modelnew->stu_name = $stu_name;
                    $modelnew->address_line1 = $address_line1;
                    $modelnew->address_line2 = $address_line2;
                    $modelnew->address_line3 = $address_line3;
                    $modelnew->student_tp = $student_tp;
                    $modelnew->district = $district;
                    $modelnew->al_year = $al_year;
                    $modelnew->faculty_name = $faculty_name;
                    $modelnew->gurdian_name = $gurdian_name;
                    $modelnew->occupation = $occupation;
                    $modelnew->gurdian_tp = $gurdian_tp;
                    $email_id=$modelnew->csvupload();
                    if($email_id){

                        echo "<script>alert('data insert sucseesfully');</script>";

                    }

                }
                unlink($path.$filename);
                return $this->render('index',['model'=>$model]);
            }
        }else{
            return $this->render('index',['model'=>$model]);
        }
    }




}