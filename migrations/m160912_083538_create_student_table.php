<?php

use yii\db\Migration;

/**
 * Handles the creation for table `student`.
 */
class m160912_083538_create_student_table extends Migration
{
    /**
     * @inheritdoc
     */
   public function safeUp()
	{
		$tableOptions = null;
		if ( $this->db->driverName === 'mysql' )
		{
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}
		
        $this->createTable('student', array(
            'id' => 'pk',
            'stu_name' => 'varchar(70) not null',
            'address_line1' => 'varchar(20) not null',
            'address_line2' => 'varchar(20) not null',
            'address_line3' => 'varchar(20)',
            'student_tp' => 'int(10) not null',
            'district' => 'varchar(20) not null',
            'al_year' => 'int(10) not null',
            'faculty_name' => 'varchar(70) not null',
            'gurdian_name' => 'varchar(70) not null',
            'occupation' => 'varchar(40)',
            'gurdian_tp' => 'int(10) not null',
        ) ,$tableOptions);
    }
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('student');
    }
}
